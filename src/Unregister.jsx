
import {useNavigate} from "react-router-dom";

export const Unregister=(props)=>{
    const navigate = useNavigate()

    const handleSubmit = (e) => {
        }
    
        return(
        <div className="auth-form-container">
        <h2>Unregister</h2>
        <form className="unregister-form" onSubmit={handleSubmit}>
            <label>Če se želite odregistrirati, kliknite spodnji link</label>
            <br/>
        <button name="Unregisterhere" className="link-btn" onClick={()=>navigate("/")}>Odregistriraj se.</button>
        <br/>
        <label>Če se ne želite odregistrirati, kliknite nazaj</label>
        <br/>
        <button name="nazaj" className="link-btn" onClick={()=>navigate("/test")}>Nazaj</button>
            
        </form>
   
    </div>
    )
}