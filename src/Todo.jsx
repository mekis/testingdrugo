import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';

import { Card } from 'react-bootstrap'

export default function Todo({ todo, toggleTodo }) {
    function handleTodoClick() {
      toggleTodo(todo.id)
    }
    
    return (
        <div>
          <Card className="mb-3" style={{color: "#000"}}>
            <Card.Body>
                <Card.Title>
                {todo.name} {todo.date}
                </Card.Title>
                <Card.Text>
                 Location: {todo.location} and {todo.wine}
                </Card.Text>
                Clear <input name="check" type="checkbox" checked={todo.complete} onChange={handleTodoClick} />
            </Card.Body>
          </Card>
        </div>
    )
}
    
  
