import React,{useState, useRef, useEffect} from "react";
import TodoList from './TodoList'
import {useNavigate} from "react-router-dom";
import { v4 as uuidv4 } from 'uuid';
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Form } from 'react-bootstrap'
import "./Unregister";



const LOCAL_STORAGE_KEY = 'todoApp.todos'

export const Test=(props)=>{
    const navigate = useNavigate()

    const [todos, setTodos] = useState([])
    const todoNameRef = useRef()
    const todoLocation = useRef()
    const todoDate = useRef()
    const todoWine = useRef()

    useEffect(() => {
        const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
        if (storedTodos) setTodos( prevTodos => [...prevTodos, ...storedTodos] )
      }, [])
    
      useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos))
      }, [todos])
    
      function toggleTodo(id) {
        const newTodos = [...todos]
        const todo = newTodos.find(todo => todo.id === id)
        todo.complete = !todo.complete
        setTodos(newTodos)
      }
    
      function handleAddTodo(e) {
        const name = todoNameRef.current.value
        console.log(name)
        const location = todoLocation.current.value
        const date = todoDate.current.value
        const wine = todoWine.current.value
        if (name === '' && location === '' && wine === '') return
        setTodos(prevTodos => {
          return [...prevTodos, { id: uuidv4(), name: name, location: location, 
            date: date, wine:wine , complete: false}]
        })
        todoNameRef.current.value = null
        todoLocation.current.value = null
        todoDate.current.value = null
        todoWine.current.value = null
      }
    
      function handleClearTodos() {
        const newTodos = todos.filter(todo => !todo.complete)
        console.log(todos.length)
        setTodos(newTodos)
      }
    
      // eslint-disable-next-line no-unused-vars
      function handleClear() {
        window.localStorage.clear()
        console.log(<Button onClick={handleClear}>Clear</Button>)
      }
        return(
            <div className="auth-form-container">
                
                <header className="App-header" />
                <TodoList todos={todos} toggleTodo={toggleTodo} />
                <div>{todos.filter(todo => !todo.complete).length} added events </div>

                <div>
                    <Form>
                        <Form.Group>
                            <Form.Label>Title of event:</Form.Label>
                            <Form.Control ref={todoNameRef} type="text"></Form.Control>
                            <Form.Label>Location of event:</Form.Label>
                            <Form.Control ref={todoLocation} type="text" ></Form.Control>
                            <Form.Label> Date of event:</Form.Label>
                            <Form.Control ref={todoDate} type="Date"></Form.Control>
                            <Form.Label> Main wine of event:</Form.Label>
                            <Form.Control ref={todoWine} type="text"></Form.Control>
                        </Form.Group>
                        <Button name="addevent" onClick={handleAddTodo}>Add Event</Button>
                        <Button name= "clearevent" onClick={handleClearTodos}>Clear Event</Button>
                    </Form>
                    <button name="UnRegisterhere" className="link-btn" onClick={()=>navigate("/unregister")}>UnRegister here.</button>
                    <button name="Needhelp" className="link-btn" onClick={()=>navigate("/needhelp")}>Need help?</button>
                </div>
            </div>
    )
}
export default Test;
