import { fireEvent, render, screen } from '@testing-library/react';
import Test from '../../src/test';
import Todo from '../../src/Todo';
import { Unregister } from '../../src/Unregister';
//import { Register } from '../../src/Register';
import Login from './Login';
import { Needhelp } from '../../src/Needhelp';
import "./App";
import { BrowserRouter, Navigate} from 'react-router-dom';
import "./Test";

test('renders login', () => {
  render(<Login />);
  const loginButton = screen.getByText(/login/i, { selector: 'button' });
  expect(loginButton).toBeInTheDocument();
});

test('renders register', () => {
  render(<Login />);
  //const registerButton = screen.getByText(/register/i, { selector: 'button' });
  const registerButton = screen.getByRole('button', { name: 'Registerhere' })
  fireEvent.click(registerButton)
  expect(registerButton).toBeCalled();
});

test('login', () => {
    render(<Login />);
    const loginButton = screen.getByRole('button', { name: "login" })
    expect(loginButton).toBeVisible();
  });

  test('add event',async ()=>{
    render(<Test />)
    expect(screen.getByRole('button',{name: "addevent"})).toBeEnabled();
});

test('clear button', () => {
    render(<Todo />);
    const clearveent = screen.getByRole('input', { name: "check" })
    expect(clearveent).toBeChecked();
  });

test('barva1', () => {
    render(<Todo />);
    const color = 'rgb(140, 52, 30)'; 
    const color1 = screen.getByRole('card', { color: {color} });
    expect(color1).toHaveStyle(`background: ${color}`); 
});

test('add event ce klice', () => {
    render(<Todo />);
    //const registerButton = screen.getByText(/register/i, { selector: 'button' });
    const addeventklic = screen.getByRole('button', { name: 'addevent' })
    fireEvent.click(addeventklic)
    expect(addeventklic).toBeCalled();
  });


  //dva nova smiselna testa
  test('odregistriraj gumb',async ()=>{
    render(<Unregister />)
    expect(screen.getByRole('button',{name: "Unregisterhere"})).toBeEnabled();
});

test('nazaj gumb', () => {
  render(<Unregister />);
  //const registerButton = screen.getByText(/register/i, { selector: 'button' });
  const nazajButton = screen.getByRole('button', { name: 'nazaj' })
  fireEvent.click(nazajButton)
  expect(nazajButton).toBeCalled();
});

//dva nova testa za form

test('submit ce klice', () => {
  render(<Needhelp />);
  const needhelp = screen.getByRole('button', { name: 'needhelp' })
  fireEvent.click(needhelp)
  expect(needhelp).toBeCalled();
});

test('back button ce je viden', () => {
  render(<Needhelp />);
  const nazaj = screen.getByRole('button', { name: 'nazaj' })
  fireEvent.click(nazaj)
  expect(nazaj).toBeVisible();
});

it('moglo bi navigirat nazaj na stran z dgogodki', ()=> {
  render(BrowserRouter(<Needhelp />))
  fireEvent.click(screen.queryByLabelText('Back'))
  //expect(navigate).toMatch("/test")
  expect(Navigate).toHaveBeenCalledWith('/test');
});

test('ce je form label v dokumentu', () => {
  render(<Needhelp />);
  const labelform = screen.getByText(/label/i, { selector: 'Form.Label' });
  expect(labelform).toBeInTheDocument();
});